## Импорт

``` r
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

``` r
library(tidyverse)
```

    ## ── Attaching packages
    ## ───────────────────────────────────────
    ## tidyverse 1.3.2 ──

    ## ✔ ggplot2 3.3.6     ✔ purrr   0.3.5
    ## ✔ tibble  3.1.8     ✔ stringr 1.4.1
    ## ✔ tidyr   1.2.1     ✔ forcats 0.5.2
    ## ✔ readr   2.1.3     
    ## ── Conflicts ────────────────────────────────────────── tidyverse_conflicts() ──
    ## ✖ dplyr::filter() masks stats::filter()
    ## ✖ dplyr::lag()    masks stats::lag()

``` r
starwars
```

    ## # A tibble: 87 × 14
    ##    name        height  mass hair_…¹ skin_…² eye_c…³ birth…⁴ sex   gender homew…⁵
    ##    <chr>        <int> <dbl> <chr>   <chr>   <chr>     <dbl> <chr> <chr>  <chr>  
    ##  1 Luke Skywa…    172    77 blond   fair    blue       19   male  mascu… Tatooi…
    ##  2 C-3PO          167    75 <NA>    gold    yellow    112   none  mascu… Tatooi…
    ##  3 R2-D2           96    32 <NA>    white,… red        33   none  mascu… Naboo  
    ##  4 Darth Vader    202   136 none    white   yellow     41.9 male  mascu… Tatooi…
    ##  5 Leia Organa    150    49 brown   light   brown      19   fema… femin… Aldera…
    ##  6 Owen Lars      178   120 brown,… light   blue       52   male  mascu… Tatooi…
    ##  7 Beru White…    165    75 brown   light   blue       47   fema… femin… Tatooi…
    ##  8 R5-D4           97    32 <NA>    white,… red        NA   none  mascu… Tatooi…
    ##  9 Biggs Dark…    183    84 black   light   brown      24   male  mascu… Tatooi…
    ## 10 Obi-Wan Ke…    182    77 auburn… fair    blue-g…    57   male  mascu… Stewjon
    ## # … with 77 more rows, 4 more variables: species <chr>, films <list>,
    ## #   vehicles <list>, starships <list>, and abbreviated variable names
    ## #   ¹​hair_color, ²​skin_color, ³​eye_color, ⁴​birth_year, ⁵​homeworld

``` r
starwars <- starwars
```

## 1. Сколько строк в Датафрейме?

``` r
starwars %>% nrow()
```

    ## [1] 87

## 2. Сколько столбцов в датафрейме?

``` r
starwars %>% ncol()
```

    ## [1] 14

## 3. Как просмотреть примерный вид датафрейма?

``` r
starwars %>% glimpse()
```

    ## Rows: 87
    ## Columns: 14
    ## $ name       <chr> "Luke Skywalker", "C-3PO", "R2-D2", "Darth Vader", "Leia Or…
    ## $ height     <int> 172, 167, 96, 202, 150, 178, 165, 97, 183, 182, 188, 180, 2…
    ## $ mass       <dbl> 77.0, 75.0, 32.0, 136.0, 49.0, 120.0, 75.0, 32.0, 84.0, 77.…
    ## $ hair_color <chr> "blond", NA, NA, "none", "brown", "brown, grey", "brown", N…
    ## $ skin_color <chr> "fair", "gold", "white, blue", "white", "light", "light", "…
    ## $ eye_color  <chr> "blue", "yellow", "red", "yellow", "brown", "blue", "blue",…
    ## $ birth_year <dbl> 19.0, 112.0, 33.0, 41.9, 19.0, 52.0, 47.0, NA, 24.0, 57.0, …
    ## $ sex        <chr> "male", "none", "none", "male", "female", "male", "female",…
    ## $ gender     <chr> "masculine", "masculine", "masculine", "masculine", "femini…
    ## $ homeworld  <chr> "Tatooine", "Tatooine", "Naboo", "Tatooine", "Alderaan", "T…
    ## $ species    <chr> "Human", "Droid", "Droid", "Human", "Human", "Human", "Huma…
    ## $ films      <list> <"The Empire Strikes Back", "Revenge of the Sith", "Return…
    ## $ vehicles   <list> <"Snowspeeder", "Imperial Speeder Bike">, <>, <>, <>, "Imp…
    ## $ starships  <list> <"X-wing", "Imperial shuttle">, <>, <>, "TIE Advanced x1",…

## 4. Сколько уникальных рас персонажей (species) представлено в данных?

``` r
unique(starwars$species) %>% length()
```

    ## [1] 38

## 5. Найти самого высокого персонажа.

``` r
starwars[which.max(starwars$height),]$name
```

    ## [1] "Yarael Poof"

## 6. Найти всех персонажей ниже 170

``` r
starwars[which(starwars$height<170),]$name
```

    ##  [1] "C-3PO"                 "R2-D2"                 "Leia Organa"          
    ##  [4] "Beru Whitesun lars"    "R5-D4"                 "Yoda"                 
    ##  [7] "Mon Mothma"            "Wicket Systri Warrick" "Nien Nunb"            
    ## [10] "Watto"                 "Sebulba"               "Shmi Skywalker"       
    ## [13] "Dud Bolt"              "Gasgano"               "Ben Quadinaros"       
    ## [16] "Cordé"                 "Barriss Offee"         "Dormé"                
    ## [19] "Zam Wesell"            "Jocasta Nu"            "Ratts Tyerell"        
    ## [22] "R4-P17"                "Padmé Amidala"

## 7. Подсчитать ИМТ (индекс массы тела) для всех персонажей. ИМТ подсчитать по формуле 𝐼 = 𝑚/ℎ^2 , где 𝑚

– масса (weight), а ℎ – рост (height).

``` r
starwars$IMT <- c(starwars$mass / (((starwars$height/100) ^ 2)))
data.frame(starwars$name, starwars$IMT)
```

    ##            starwars.name starwars.IMT
    ## 1         Luke Skywalker     26.02758
    ## 2                  C-3PO     26.89232
    ## 3                  R2-D2     34.72222
    ## 4            Darth Vader     33.33007
    ## 5            Leia Organa     21.77778
    ## 6              Owen Lars     37.87401
    ## 7     Beru Whitesun lars     27.54821
    ## 8                  R5-D4     34.00999
    ## 9      Biggs Darklighter     25.08286
    ## 10        Obi-Wan Kenobi     23.24598
    ## 11      Anakin Skywalker     23.76641
    ## 12        Wilhuff Tarkin           NA
    ## 13             Chewbacca     21.54509
    ## 14              Han Solo     24.69136
    ## 15                Greedo     24.72518
    ## 16 Jabba Desilijic Tiure    443.42857
    ## 17        Wedge Antilles     26.64360
    ## 18      Jek Tono Porkins     33.95062
    ## 19                  Yoda     39.02663
    ## 20             Palpatine     25.95156
    ## 21             Boba Fett     23.35095
    ## 22                 IG-88     35.00000
    ## 23                 Bossk     31.30194
    ## 24      Lando Calrissian     25.21625
    ## 25                 Lobot     25.79592
    ## 26                Ackbar     25.61728
    ## 27            Mon Mothma           NA
    ## 28          Arvel Crynyd           NA
    ## 29 Wicket Systri Warrick     25.82645
    ## 30             Nien Nunb     26.56250
    ## 31          Qui-Gon Jinn     23.89326
    ## 32           Nute Gunray     24.67038
    ## 33         Finis Valorum           NA
    ## 34         Jar Jar Binks     17.18034
    ## 35          Roos Tarpals     16.34247
    ## 36            Rugor Nass           NA
    ## 37              Ric Olié           NA
    ## 38                 Watto           NA
    ## 39               Sebulba     31.88776
    ## 40         Quarsh Panaka           NA
    ## 41        Shmi Skywalker           NA
    ## 42            Darth Maul     26.12245
    ## 43           Bib Fortuna           NA
    ## 44           Ayla Secura     17.35892
    ## 45              Dud Bolt     50.92802
    ## 46               Gasgano           NA
    ## 47        Ben Quadinaros     24.46460
    ## 48            Mace Windu     23.76641
    ## 49          Ki-Adi-Mundi     20.91623
    ## 50             Kit Fisto     22.64681
    ## 51             Eeth Koth           NA
    ## 52            Adi Gallia     14.76843
    ## 53           Saesee Tiin           NA
    ## 54           Yarael Poof           NA
    ## 55              Plo Koon     22.63468
    ## 56            Mas Amedda           NA
    ## 57          Gregar Typho     24.83565
    ## 58                 Cordé           NA
    ## 59           Cliegg Lars           NA
    ## 60     Poggle the Lesser     23.88844
    ## 61       Luminara Unduli     19.44637
    ## 62         Barriss Offee     18.14487
    ## 63                 Dormé           NA
    ## 64                 Dooku     21.47709
    ## 65   Bail Prestor Organa           NA
    ## 66            Jango Fett     23.58984
    ## 67            Zam Wesell     19.48696
    ## 68       Dexter Jettster     26.01775
    ## 69               Lama Su     16.78076
    ## 70               Taun We           NA
    ## 71            Jocasta Nu           NA
    ## 72         Ratts Tyerell     24.03461
    ## 73                R4-P17           NA
    ## 74            Wat Tambor     12.88625
    ## 75              San Hill           NA
    ## 76              Shaak Ti     17.99015
    ## 77              Grievous     34.07922
    ## 78               Tarfful     24.83746
    ## 79       Raymus Antilles     22.35174
    ## 80             Sly Moore     15.14960
    ## 81            Tion Medon     18.85192
    ## 82                  Finn           NA
    ## 83                   Rey           NA
    ## 84           Poe Dameron           NA
    ## 85                   BB8           NA
    ## 86        Captain Phasma           NA
    ## 87         Padmé Amidala     16.52893

## 8. Найти 10 самых “вытянутых” персонажей. “Вытянутость” оценить по отношению массы (mass) к росту

(height) персонажей.

``` r
starwars$tall <- starwars$mass/starwars$height
tallest <- arrange(starwars[which(!is.na(starwars$tall)),],by = tall,)
tallest<- tallest[seq(dim(tallest)[1],1),]
topTall <- head(tallest,n=10)
data.frame(topTall$name, topTall$tall)
```

    ##             topTall.name topTall.tall
    ## 1  Jabba Desilijic Tiure    7.7600000
    ## 2               Grievous    0.7361111
    ## 3                  IG-88    0.7000000
    ## 4              Owen Lars    0.6741573
    ## 5            Darth Vader    0.6732673
    ## 6       Jek Tono Porkins    0.6111111
    ## 7                  Bossk    0.5947368
    ## 8                Tarfful    0.5811966
    ## 9        Dexter Jettster    0.5151515
    ## 10             Chewbacca    0.4912281

## 9. Найти средний возраст персонажей каждой расы вселенной Звездных войн.

``` r
starwars$age <- 2022 - starwars$birth_year
races <- unique(starwars$species)
for (race in races)
{
  print(race)
  print(mean( starwars[which(starwars$species==race),]$age , na.rm=TRUE ))
}
```

    ## [1] "Human"
    ## [1] 1968.588
    ## [1] "Droid"
    ## [1] 1968.667
    ## [1] "Wookiee"
    ## [1] 1822
    ## [1] "Rodian"
    ## [1] 1978
    ## [1] "Hutt"
    ## [1] 1422
    ## [1] "Yoda's species"
    ## [1] 1126
    ## [1] "Trandoshan"
    ## [1] 1969
    ## [1] "Mon Calamari"
    ## [1] 1981
    ## [1] "Ewok"
    ## [1] 2014
    ## [1] "Sullustan"
    ## [1] NaN
    ## [1] "Neimodian"
    ## [1] NaN
    ## [1] "Gungan"
    ## [1] 1970
    ## [1] NA
    ## [1] NaN
    ## [1] "Toydarian"
    ## [1] NaN
    ## [1] "Dug"
    ## [1] NaN
    ## [1] "Zabrak"
    ## [1] 1968
    ## [1] "Twi'lek"
    ## [1] 1974
    ## [1] "Vulptereen"
    ## [1] NaN
    ## [1] "Xexto"
    ## [1] NaN
    ## [1] "Toong"
    ## [1] NaN
    ## [1] "Cerean"
    ## [1] 1930
    ## [1] "Nautolan"
    ## [1] NaN
    ## [1] "Tholothian"
    ## [1] NaN
    ## [1] "Iktotchi"
    ## [1] NaN
    ## [1] "Quermian"
    ## [1] NaN
    ## [1] "Kel Dor"
    ## [1] 2000
    ## [1] "Chagrian"
    ## [1] NaN
    ## [1] "Geonosian"
    ## [1] NaN
    ## [1] "Mirialan"
    ## [1] 1973
    ## [1] "Clawdite"
    ## [1] NaN
    ## [1] "Besalisk"
    ## [1] NaN
    ## [1] "Kaminoan"
    ## [1] NaN
    ## [1] "Aleena"
    ## [1] NaN
    ## [1] "Skakoan"
    ## [1] NaN
    ## [1] "Muun"
    ## [1] NaN
    ## [1] "Togruta"
    ## [1] NaN
    ## [1] "Kaleesh"
    ## [1] NaN
    ## [1] "Pau'an"
    ## [1] NaN

## 10. Найти самый распространенный цвет глаз персонажей вселенной Звездных войн.

``` r
by_colour <-starwars %>%group_by(eye_color)
print( head(arrange(by_colour%>% tally(),desc(n),),1) )
```

    ## # A tibble: 1 × 2
    ##   eye_color     n
    ##   <chr>     <int>
    ## 1 brown        21

## 11. Подсчитать среднюю длину имени в каждой расе вселенной Звездных войн

``` r
starwars %>% filter(!is.na(species)) %>% group_by(species) %>% summarise(length=mean(nchar(name)))
```

    ## # A tibble: 37 × 2
    ##    species   length
    ##    <chr>      <dbl>
    ##  1 Aleena     13   
    ##  2 Besalisk   15   
    ##  3 Cerean     12   
    ##  4 Chagrian   10   
    ##  5 Clawdite   10   
    ##  6 Droid       4.83
    ##  7 Dug         7   
    ##  8 Ewok       21   
    ##  9 Geonosian  17   
    ## 10 Gungan     11.7 
    ## # … with 27 more rows
